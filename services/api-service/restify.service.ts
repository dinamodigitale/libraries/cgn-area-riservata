import { ApiService } from './api.service'
import { Observable } from 'rxjs'

export abstract class Restify<T> {
  abstract basePath: string

  constructor(protected apiService: ApiService) {}

  index(queryString: any = ''): Observable<any> {
    return this.apiService.get(this.basePath + queryString)
  }

  get(id): Observable<T> {
    return this.apiService.get(`${this.basePath}/${id}`)
  }
}
