import { Injectable } from '@angular/core'
import { tap, map } from 'rxjs/operators'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { config } from 'src/environments/environment'
import { BehaviorSubject, Observable } from 'rxjs'

interface I18nInterface {
  _i18n: boolean
  it?: any
  en: any
}

export interface UserInterface {
  biography: I18nInterface
  createdAt: string
  email: string
  id: string
  name: string
  referente: string
  roles: { name: string }[]
  surname: string
  thumbnail: string
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  private apiConfig = config.api
  private loggedUser = new BehaviorSubject<UserInterface | undefined>(undefined)

  constructor(private http: HttpClient) { }

  login(email: string, password: string) {
    return this.http
      .post(
        `${this.apiConfig.authPath}/auth`,
        {
          email,
          password
        },
        {
          observe: 'response'
        }
      )
      .pipe(tap(res => this.saveUserInfo(res.body)))
  }

  setNewPassword(data: any) {
    return this.http.post(`${this.apiConfig.authPath}/auth/set-password`,data);
  }
  

  resetPassword(email: string) {
    return this.http.post(`${this.apiConfig.authPath}/auth/reset`,{email});
  }

  private saveUserInfo(user): void {
    localStorage.setItem('userInfoFE', JSON.stringify(user))
    this.loggedUser.next(user)
  }

 removeUserInfo(): void {
    console.log('User info flushed')
    localStorage.removeItem('userInfoFE')
    localStorage.removeItem('tokenFE')
    localStorage.removeItem('uidFE')
    this.loggedUser.next(undefined)
  }

  getUserInfo() {
    return JSON.parse(localStorage.getItem('userInfoFE'))
  }

  logout() {
    // force header in logout
    const headers = new HttpHeaders({
      Authorization: `Bearer ${localStorage.getItem('tokenFE')}`,
      'X-UID': `${localStorage.getItem('uidFE')}`
    })
    return this.http
      .delete(`${this.apiConfig.authPath}/auth/logout`, { headers })
      .pipe(
        tap(res => {
          this.removeUserInfo()
        })
      )
  }

  public isLoggedIn() {
    const token = localStorage.getItem('tokenFE');
    const uid = localStorage.getItem('uidFE')
    if(!token !! || !uid) {
      return new Observable((observer) => {
        console.log('Ignoring token verification')
        observer.next(false);
      })
    }
    console.log('Verifying user token')
    return this.http
      .post(`${this.apiConfig.authPath}/auth/verify`, {
        token,
        uid
      })
      .pipe(
        tap(res => this.saveUserInfo(res), err => this.removeUserInfo()),
        map(user => {
          console.log('Token verification', !!user['_id'] ? 'success' : 'failed')
          return !!user['_id']
        })
      )
  }

  get getLoggedUser() {
    return this.loggedUser.asObservable()
  }

  isAdministrator() {
    return JSON.parse(localStorage.getItem('userInfoFE')).role === 'admin'
  }
}
