import { InjectionToken } from '@angular/core';

export const AUTH_INTERCEPTOR_OPTIONS = new InjectionToken('AUTH_INTERCEPTOR_OPTIONS');
