import { Component, OnInit } from '@angular/core'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { AuthService } from '@be-lib/services/auth-service.service'
import { MatSnackBar } from '@angular/material/snack-bar'
import { StateService } from '@uirouter/core'

@Component({
  selector: 'dbe-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit {
  form: FormGroup
  errors: Array<any> = []

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private snackbar: MatSnackBar,
    private stateService: StateService
  ) {
    const params = this.stateService.params
    if (!params.email || !params.token) {
      this.stateService.go('app.public.login')
    }
  }

  ngOnInit(): void {
    this.form = this.fb.group({
      password: ['', Validators.required],
      passwordConfirmation: ['', Validators.required],
    })
  }

  resetPassword() {
    if (this.form.value.password && this.form.value.passwordConfirmation) {
      this.authService
        .validatePasswordReset(
          this.stateService.params.email,
          this.stateService.params.token,
          this.form.value.password,
          this.form.value.passwordConfirmation
        )
        .subscribe(
          (res) => {
            this.stateService.go('app.public.login')
            this.snackbar.open('Reset password avvenuto con successo!')
          },
          (err) => {
            console.log(err)
            this.snackbar.open('Si è verificato un problema..')
          }
        )
    }
  }
}
