import { Component, OnInit } from '@angular/core'
import { FormGroup } from '@angular/forms'
import { FormBuilder } from '@angular/forms'
import { Validators } from '@angular/forms'
import { AuthService } from "../../../services/auth-service.service";
import { StateService } from '@uirouter/core'

@Component({
  selector: 'dinamo-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  form: FormGroup
  errors: Array<any> = []
  currentLocale: any

  username = ''
  password = ''
  constructor(
    private state: StateService,
    private fb: FormBuilder,
    private authService: AuthService,
  ) {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    })

    this.authService.isLoggedIn().subscribe(
      () => {
        console.log('Authenticated, redirect to dashboard')
        this.state.go('app.private.dashboard')
      },
      () => {
        console.warn('Not authenticated, need to login')
      }
    )
  }

  setLocale(locale) {
    this.currentLocale = locale
  }

  login() {
    this.authService.login(this.username, this.password).subscribe(res => {
      console.log("res after login", res)
      if (res.body.role === 'company') {
        this.state.go('app.private.company-reserved')
      } else {
        this.state.go('app.private')
      }
    }, err => console.error(err))
    // const val = this.form.value

    // if (val.email && val.password) {
    //   this.authService.login(val.email, val.password).subscribe(
    //     (res) => {
    //       window.localStorage.privacy = 'true'
    //       this.state.go('app.private.dashboard')
    //     },
    //     (err) => {
    //       console.error(err)
          
    //     }
    //   )
    // }
  }
}
