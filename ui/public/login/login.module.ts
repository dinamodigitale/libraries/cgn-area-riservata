import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { LoginComponent } from './login.component'
import { AuthService } from '../../../services/____auth.service'
import { UIRouterModule } from '@uirouter/angular'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    UIRouterModule.forChild({
      states: [
        {
          name: 'app.public.login',
          url: '/login',
          component: LoginComponent,
        },
      ],
    }),
  ],
  declarations: [LoginComponent],
  providers: [AuthService],
})
export class LoginModule {}
