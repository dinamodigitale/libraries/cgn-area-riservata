import { NgModule } from '@angular/core'

import { UIRouterModule } from '@uirouter/angular'
import { BrowserModule } from '@angular/platform-browser'

import { PublicComponent } from './public.component'
import { LoginModule } from './login/login.module'
// import { ResetPasswordModule } from 'src/app/ui/reset-password/reset-password.module'
// import { PageNotFoundModule } from './page-not-found/page-not-found.module'
// import { LoginModule } from './login/login.module'
// import { ForgotPasswordModule } from './forgot-password/forgot-password.module'
// import { ResetPasswordModule } from './reset-password/reset-password.module'

@NgModule({
  imports: [
    UIRouterModule.forChild({
      states: [
        {
          name: 'app.public',
          url: '',
          component: PublicComponent,
        },
      ],
    }),
    BrowserModule,
    LoginModule,
    // PageNotFoundModule,
    // ForgotPasswordModule,
    // ResetPasswordModule
  ],
  
  declarations: [PublicComponent],
})
export class PublicModule {}
