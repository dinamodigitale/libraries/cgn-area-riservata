import { NgModule, Injector } from "@angular/core";
import { PrivateComponent } from "./private.component";
import { UIRouterModule, UIRouter, RejectType } from "@uirouter/angular";

import { AuthService } from "../../services/auth-service.service";
import { BrowserModule } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ApiService } from '@area-riservata/services/api-service/api.service';

export function authConfig(router: UIRouter, injector: Injector) {
  const authService = injector.get(AuthService);
  console.log("PRIVATE MODULE", authService)
  router.transitionService.onStart(
    {
      to: "**.private.**",
    },
    (trans) => {
      return authService.isLoggedIn().toPromise();
    }
  );

  router.transitionService.onError(
    {
      to: "**.private.**",
    },
    (trans) => {
      console.log("ciaone bello")
      const rejection = trans.error();
      if (
        rejection.type === RejectType.ERROR &&
        rejection.detail &&
        rejection.detail.status &&
        rejection.detail.status === 401
      ) {
        // === 6
        alert(
          "401 unauthorized response from the server, redirecting to login"
        );
        console.warn(
          "401 unauthorized response from the server, redirecting to login"
        );
        authService.removeUserInfo();
        trans.router.stateService.go("app.public.login");
      } else {
        console.log("Transition error", rejection);
        return true;
      }
    }
  );
}

@NgModule({
  imports: [
    BrowserModule,
    UIRouterModule.forChild({
      states: [
        {
          name: "app.private",
          url: "/private",
          component: PrivateComponent,
        },
      ],
      config: authConfig,
    }),
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [PrivateComponent],
  providers: [ApiService],
})
export class PrivateModule {}
