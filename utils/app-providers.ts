import { HTTP_INTERCEPTORS } from '@angular/common/http'
import { ApiService } from 'src/app/services/api-service/api.service';
import { AuthInterceptor } from "../interceptors/auth/auth-interceptor";
import { AUTH_INTERCEPTOR_OPTIONS } from '../interceptors/auth/auth-interceptor-options'
// import { AuthInterceptor } from '@area-riservata/interceptors/auth/auth-interceptor'
// import { AUTH_INTERCEPTOR_OPTIONS } from '@area-riservata/interceptors/auth/auth-interceptor-options'

const AppProviders = [
  ApiService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true,
  },
  {
    provide: AUTH_INTERCEPTOR_OPTIONS,
    useValue: {
      whitelist: [new RegExp(".+/api/v1/frontend.+")],
      blacklist: [new RegExp(".+/api/v*/auth/.+")],
    },
  },
];

export default AppProviders
